'use strict'

const _ = require('lodash')
const axios = require('axios')

const Utils = require('./utils')

describe('Roles API', function () {
  let url = 'http://localhost:8080'
  let conf = {}

  let testData = {
    post: {
      data: {
        name: `xxx${Date.now()}`,
        account: 'xxx',
        permissions: [
          {
            accessLevel: 'admin',
            resource: 'devices'
          }
        ]
      }
    },
    get: {
      readId: 'b98fcc45-e55f-5c0f-a854-348cfb912ec9',
      account: '059fea35-fe7d-5017-a80b-689733af01dc',
      query: {
        basic: '?q=Device Admin',
        select: '?q=Device Admin&select=permissions',
        deselect: '?q=Device Admin&select=-permissions',
        sortAsc: '?q=Device&sort=name',
        sortDesc: '?q=Device&sort=-name',
        limit: '?q=&limit=3&sort=name&select=name',
        page: '?q=&limit=3&sort=name&select=name&page=2',
        expand: '?select=-permissions&expand=account account.owner aa.bb',
      }
    }
  }

  let eResult = {
    sort: ['Device Admin', 'Device Manager'],
    page: ['Account Manager', 'Dashboard Viewer']
  }

  describe('# GET', function () {
    it('should retrieve role record by id', function (done) {
      axios.get(`${url}/${testData.get.readId}`, conf).then(ret => {
        if (ret.status === 200 && ret.data._id === testData.get.readId) {
          done()
        }
      }).catch(err => {
        Utils.handleErr(err, done)
      })
    })

    it('should retrieve role record by id and expand account', function (done) {
      axios.get(`${url}/${testData.get.readId}${testData.get.query.expand}`, conf).then(ret => {
        if (ret.status === 200 && ret.data._id === testData.get.readId) {
          done()
        }
      }).catch(err => {
        Utils.handleErr(err, done)
      })
    })

    it('should query or search roles', function (done) {
      axios.get(`${url}/${testData.get.query.basic}`, conf).then(ret => {
        if (ret.status === 200 && ret.data.totalRecords > 0) {
          done()
        }
      }).catch(Utils.handleErr)
    })

    it('should query or search roles - select', function (done) {
      axios.get(`${url}/${testData.get.query.select}`, conf).then(ret => {
        if (ret.status === 200 && ret.data.totalRecords === 1 && !ret.data.data[0].name) {
          done()
        }
      }).catch(Utils.handleErr)
    })

    it('should query or search roles - deselect', function (done) {
      axios.get(`${url}/${testData.get.query.deselect}`, conf).then(ret => {
        if (ret.status === 200 && ret.data.totalRecords === 1 && !ret.data.data[0].permissions) {
          done()
        }
      }).catch(Utils.handleErr)
    })

    it('should query or search roles - sort ascending', function (done) {
      axios.get(`${url}/${testData.get.query.sortAsc}`, conf).then(ret => {
        if (ret.status === 200 && ret.data.totalRecords > 1) {
          if (ret.data.data[0].name === eResult.sort[0] && ret.data.data[1].name === eResult.sort[1]) {
            done()
          }
        }
      }).catch(Utils.handleErr)
    })

    it('should query or search roles - sort desscending', function (done) {
      axios.get(`${url}/${testData.get.query.sortDesc}`, conf).then(ret => {
        if (ret.status === 200 && ret.data.totalRecords > 1) {
            if (ret.data.data[1].name === eResult.sort[0] && ret.data.data[0].name === eResult.sort[1]) {
            done()
          }
        }
      }).catch(Utils.handleErr)
    })

    it('should query or search roles - limit (page 1)', function (done) {
      axios.get(`${url}/${testData.get.query.limit}`, conf).then(ret => {
        if (ret.status === 200 && ret.data.totalRecords > 1) {
          if (ret.data.data[0].name === eResult.page[0]) {
            done()
          }
        }
      }).catch(Utils.handleErr)
    })

    it('should query or search roles - limit (page 2)', function (done) {
      axios.get(`${url}/${testData.get.query.page}`, conf).then(ret => {
        if (ret.status === 200 && ret.data.totalRecords > 1) {
          if (ret.data.data[0].name === eResult.page[1]) {
            done()
          }
        }
      }).catch(Utils.handleErr)
    })
  })

  describe('# POST', function () {
    it('should create a new custom role', function (done) {
      axios.post(`${url}/`, testData.post.data, conf).then(ret => {
        if (ret.status === 201 && ret.data.name === testData.post.data.name) {
          testData.post.data._id = ret.data._id
          done()
        }
      }).catch(err => {
        Utils.handleErr(err, done)
      })
    })
  })

  describe('# PATCH', function () {
    it('should patch or partially update values for an existing role', function (done) {
      testData.post.data.name = `xxx patch ${Date.now()}`
      axios.patch(`${url}/${testData.post.data._id}`, testData.post.data, conf).then(ret => {
        if (ret.status === 200) {
          done()
        }
      }).catch(err => {
        Utils.handleErr(err, done)
      })
    })
  })

  describe('# PUT', function () {
    it('should update/overwrite values for an existing role', function (done) {
      testData.post.data.name = `xxx put ${Date.now()}`
      delete testData.post.data.isStandard

      axios.put(`${url}/${testData.post.data._id}`, testData.post.data, conf).then(ret => {
        if (ret.status === 200 && _.isNil(ret.data.isStandard)) {
          done()
        }
      }).catch(err => {
        Utils.handleErr(err, done)
      })
    })
  })

  describe('# DEL', function () {
    it('should delete an existing role', function (done) {
      axios.delete(`${url}/${testData.post.data._id}`, conf).then(ret => {
        if (ret.status === 204) {
          testData.post.data._id = ret.data._id
          done()
        }
      }).catch(err => {
        Utils.handleErr(err, done)
      })
    })
  })
})
