module.exports = {
  get: {
    readId: '', // to be filled by POST
  },
  post: {
    name: 'Dummy Name',
    email: 'abc@xyz.com'
  },
  query: {
    basic: {q: 'Dummy'},
    expand: {q: 'Dummy', expand: 'owner'},
    select: {q: 'Dummy', select: 'name email'},
    deselect: {q: 'Dummy', select: '-name -email'},
    sortAsc: {q: 'Dummy', sort: 'name', select: 'name'},
    sortDesc: {q: 'Dummy', sort: '-name', select: 'name'},
    limit: {q: 'Dummy', limit: 1, sort: 'name', select: 'name'},
    page: {q: 'Dummy', limit: 1, sort: 'name', select: 'name', page: 2}
  },
  result: {
    page: [{name: 'xxx'}, {name: 'xxx'}],
    sort: [{name: 'Dummy - Edited 2'}, {name: 'Dummy for search'}]
  }
}