'use strict'

const RESOURCE = 'resources'
global.Promise = require('bluebird')

const _ = require('lodash')
const Utils = require('./utils')
const should = require('should')

const supertest = require('supertest')
const request = supertest('http://localhost:8080')

describe('Roles API', function () {
  let user = {}
  let config = {}
  let utils = new Utils(RESOURCE)

  let _data = {
    get: {
      readId: 'channels',
    },
    query: {
      basic: {q: 'comm'},
      sortAsc: {q: 'device-c', sort: '_id'},
      sortDesc: {q: 'device-c', sort: '-_id'},
      select: {q: 'comm', select: 'accessLevels'},
      deselect: {q: 'comm', select: '-accessLevels'},
      limit: {q: '', limit: 3, select: '_id'},
      page: {q: '', limit: 3, select: '_id', page: 2}
    },
    result: {
      page: [{_id: 'accounts'}, {_id: 'command-relays'}],
      sort: [{_id: 'device-certs'}, {_id: 'device-commands'}]
    }
  }

  before(function (done) {
    utils.init('admin').then(options => {
      config = options.config
      user = options.user
      done()
    }).catch(done)
  })

  after(function (done) {
    done()
  })

  describe('# API GET', function () {
    it('should retrieve record by id - naked', function (done) {
      request.get(`/${_data.get.readId}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should.equal(res.body._id, _data.get.readId)
          done()
        })
    })
    
    it('should retrieve record by id - select', function (done) {
      request.get(`/${_data.get.readId}?${utils.toQueryStr(_data.query.select)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should.exist(res.body.accessLevels)
          should.equal(res.body._id, _data.get.readId)
          done()
        })
    })

    it('should retrieve record by id - deselect', function (done) {
      request.get(`/${_data.get.readId}?${utils.toQueryStr(_data.query.deselect)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should.not.exist(res.body.accessLevels)
          should.equal(res.body._id, _data.get.readId)
          done()
        })
    })

    it('should query or search - naked', function (done) {
      request.get(`${utils.toQueryStr(_data.query.basic)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body.data).be.an.Array()
          should(res.body.totalPages).be.a.Number()
          should(res.body.docsPerPage).be.a.Number()
          should(res.body.currentPage).be.a.Number()
          should(res.body.totalRecords).be.a.Number()
          done()
        })
    })

    it('should query or search - select', function (done) {
      request.get(`${utils.toQueryStr(_data.query.select)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body.data).be.an.Array()
          should(res.body.totalPages).be.a.Number()
          should(res.body.docsPerPage).be.a.Number()
          should(res.body.currentPage).be.a.Number()
          should(res.body.totalRecords).be.a.Number()
          should.exist(res.body.data[0].accessLevels)
          done()
        })
    })

    it('should query or search - deselect', function (done) {
      request.get(`${utils.toQueryStr(_data.query.deselect)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body.data).be.an.Array()
          should(res.body.totalPages).be.a.Number()
          should(res.body.docsPerPage).be.a.Number()
          should(res.body.currentPage).be.a.Number()
          should(res.body.totalRecords).be.a.Number()
          should.not.exist(res.body.data[0].accessLevels)
          done()
        })
    })

    it('should query or search - sort ascending', function (done) {
      request.get(`${utils.toQueryStr(_data.query.sortAsc)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body.data).be.an.Array()
          should(res.body.totalPages).be.a.Number()
          should(res.body.docsPerPage).be.a.Number()
          should(res.body.currentPage).be.a.Number()
          should(res.body.totalRecords).be.a.Number()
          should.equal(res.body.data[0]._id, _data.result.sort[0]._id)
          should.equal(res.body.data[1]._id, _data.result.sort[1]._id)
          done()
        })
    })

    it('should query or search - sort desscending', function (done) {
      request.get(`${utils.toQueryStr(_data.query.sortDesc)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body.data).be.an.Array()
          should(res.body.totalPages).be.a.Number()
          should(res.body.docsPerPage).be.a.Number()
          should(res.body.currentPage).be.a.Number()
          should(res.body.totalRecords).be.a.Number()
          should.equal(res.body.data[0]._id, _data.result.sort[1]._id)
          should.equal(res.body.data[1]._id, _data.result.sort[0]._id)
          done()
        })
    })

    it('should query or search - limit (page 1)', function (done) {
      request.get(`${utils.toQueryStr(_data.query.limit)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body.data).be.an.Array()
          should(res.body.totalPages).be.a.Number()
          should(res.body.docsPerPage).be.a.Number()
          should(res.body.currentPage).be.a.Number()
          should(res.body.totalRecords).be.a.Number()
          should.equal(res.body.data[0]._id, _data.result.page[0]._id)
          done()
        })
    })

    it('should query or search - limit (page 2)', function (done) {
      request.get(`${utils.toQueryStr(_data.query.page)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body.data).be.an.Array()
          should(res.body.totalPages).be.a.Number()
          should(res.body.docsPerPage).be.a.Number()
          should(res.body.currentPage).be.a.Number()
          should(res.body.totalRecords).be.a.Number()
          should.equal(res.body.data[0]._id, _data.result.page[1]._id)
          done()
        })
    })
  })

  describe('# Services', function () {
    it('should retrieve record by id - naked', function (done) {
      let options = {
        filter: {
          _id: _data.get.readId
        }
      }

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:read`, options)
        .then(doc => {
          should.exist(doc._id)
          should.equal(_data.get.readId, doc._id)

          done()
        }).catch(done)
    })

    it('should retrieve record by id - select', function (done) {
      let options = {
        filter: {
          _id: _data.get.readId
        }
      }

      options = Object.assign(options, _data.query.select)

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:read`, options)
        .then(doc => {
          should.exist(doc._id)
          should.exist(doc.accessLevels)
          should.equal(_data.get.readId, doc._id)
          done()
        }).catch(done)
    })

    it('should retrieve record by id - deselect', function (done) {
      let options = {
        filter: {
          _id: _data.get.readId
        }
      }

      options = Object.assign(options, _data.query.deselect)

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:read`, options)
        .then(doc => {
          should.exist(doc._id)
          should.not.exist(doc.accessLevels)
          should.equal(_data.get.readId, doc._id)

          done()
        }).catch(done)
    })

    it('should query or search - naked', function (done) {
      let options = {}

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, options)
        .then(docs => {
          should.exist(docs.data)
          should.exist(docs.totalPages)
          should.exist(docs.currentPage)
          should.exist(docs.docsPerPage)
          should.exist(docs.totalRecords)

          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()
          
          should(docs.data).not.be.empty()
          should(docs.totalPages).be.greaterThanOrEqual(1)
          should(docs.totalRecords).be.greaterThanOrEqual(1)

          done()
        }).catch(done)
    })

    it('should query or search - select', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.select)
        .then(docs => {
          should.exist(docs.data)
          should.exist(docs.totalPages)
          should.exist(docs.currentPage)
          should.exist(docs.docsPerPage)
          should.exist(docs.totalRecords)
          should.exist(docs.data[0].accessLevels)
          done()
        }).catch(done)
    })

    it('should query or search - deselect', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.deselect)
        .then(docs => {
          should.exist(docs.data)
          should.exist(docs.totalPages)
          should.exist(docs.currentPage)
          should.exist(docs.docsPerPage)
          should.exist(docs.totalRecords)
          should.not.exist(docs.data[0].accessLevels)
          done()
        }).catch(done)
    })

    it('should query or search - sort ascending', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.sortAsc)
        .then(docs => {
          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()
          
          should.equal(docs.data[0]._id, _data.result.sort[0]._id)
          should.equal(docs.data[1]._id, _data.result.sort[1]._id)

          done()
        }).catch(done)
    })

    it('should query or search - sort descending', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.sortDesc)
        .then(docs => {
          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()
          
          should.equal(docs.data[0]._id, _data.result.sort[1]._id)
          should.equal(docs.data[1]._id, _data.result.sort[0]._id)

          done()
        }).catch(done)
    })

    it('should query or search - limit (page 1)', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.limit)
        .then(docs => {
          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()
          
          should.equal(docs.data[0]._id, _data.result.page[0]._id)

          done()
        }).catch(done)
    })

    it('should query or search - limit (page 2)', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.page)
        .then(docs => {
          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()

          should.equal(docs.data[0]._id, _data.result.page[1]._id)

          done()
        }).catch(done)
    })
    
    it(`should list all without pagination`, function (done) {
      let options = { listOnly: true }

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, options)
        .then(docs => {
          should(docs).be.an.Array()
          should(docs).not.be.empty()
          done()
        }).catch(done)
    })

    it(`should count`, function (done) {
      let options = {}

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:count`, options)
        .then(result => {
          should(result.count).be.a.Number()
          should(result.count).be.greaterThanOrEqual(1)
          done()
        }).catch(done)
    })
  })
})
