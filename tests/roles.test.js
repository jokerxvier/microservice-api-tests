'use strict'

const RESOURCE = 'roles'
global.Promise = require('bluebird')

const _ = require('lodash')
const Utils = require('./utils')
const should = require('should')

const supertest = require('supertest')
const request = supertest('http://localhost:8080')

describe('Roles API', function () {
  let user = {}
  let config = {}
  let utils = new Utils(RESOURCE)

  let _data = {
    get: {
      readId: '', // to be filled by POST
    },
    post: {
      account: '70db64c8-90ba-52ec-a4c4-bbc64ffd3f4e',
      isStandard: false,
      name: 'Dummy Role',
      permissions: [{
        accessLevel : 'none', 
        resource : 'accounts'
      }]
    },
    query: {
      basic: {q: 'Dashboard'},
      sortAsc: {q: 'Dashboard', sort: 'name', select: 'name'},
      sortDesc: {q: 'Dashboard', sort: '-name', limit: 2, select: 'name'},
      select: {q: 'Dashboard', select: 'isStandard'},
      deselect: {q: 'Dashboard', select: '-isStandard'},
      limit: {q: '', limit: 2, sort: 'name', select: 'name'},
      page: {q: '', limit: 2, sort: 'name', select: 'name', page: 2}
    },
    result: {
      page: [{name: 'Security Manager'}, {name: 'Security Tester'}],
      sort: [{name: 'Dashboard Tester'}, {name: 'Dashboard Viewer'}]
    }
  }

  before(function (done) {
    utils.init('admin').then(options => {
      config = options.config
      user = options.user
      done()
    }).catch(done)
  })

  after(function (done) {
    done()
  })

  /* describe('# Services', function () {
    it('should retrieve record by id - naked', function (done) {
      let options = {
        user: user,
        filter: {
          _id: _data.get.readId
        }
      }

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:read`, options)
        .then(doc => {
          should.exist(doc._id)
          should.equal(_data.get.readId, doc._id)
          done()
        }).catch(done)
    })

    it('should retrieve record by id - select', function (done) {
      let options = {
        user: user,
        filter: {
          _id: _data.get.readId
        }
      }

      options = Object.assign(options, _data.query.select)

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:read`, options)
        .then(doc => {
          should.exist(doc._id)
          should.exist(doc.isStandard)
          should.equal(_data.get.readId, doc._id)
          done()
        }).catch(done)
    })

    it('should retrieve record by id - deselect', function (done) {
      let options = {
        user: user,
        filter: {
          _id: _data.get.readId
        }
      }

      options = Object.assign(options, _data.query.deselect)

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:read`, options)
        .then(doc => {
          should.exist(doc._id)
          should.not.exist(doc.isStandard)
          should.equal(_data.get.readId, doc._id)

          done()
        }).catch(done)
    })

    it('should query or search - naked', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, { user })
        .then(docs => {
          should.exist(docs.data)
          should.exist(docs.totalPages)
          should.exist(docs.currentPage)
          should.exist(docs.docsPerPage)
          should.exist(docs.totalRecords)

          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()
          
          should(docs.data).not.be.empty()
          should(docs.totalPages).be.greaterThanOrEqual(1)
          should(docs.totalRecords).be.greaterThanOrEqual(1)

          done()
        }).catch(done)
    })

    it('should query or search - select', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.select, { user })
        .then(docs => {
          should.exist(docs.data)
          should.exist(docs.totalPages)
          should.exist(docs.currentPage)
          should.exist(docs.docsPerPage)
          should.exist(docs.totalRecords)
          should.exist(docs.data[0].isStandard)
          done()
        }).catch(done)
    })

    it('should query or search - deselect', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.deselect, { user })
        .then(docs => {
          should.exist(docs.data)
          should.exist(docs.totalPages)
          should.exist(docs.currentPage)
          should.exist(docs.docsPerPage)
          should.exist(docs.totalRecords)
          should.not.exist(docs.data[0].isStandard)
          done()
        }).catch(done)
    })

    it('should query or search - sort ascending', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.sortAsc, { user })
        .then(docs => {
          console.log(docs)
          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()
          
          should.equal(docs.data[0].name, _data.result.sort[0].name)
          should.equal(docs.data[1].name, _data.result.sort[1].name)

          done()
        }).catch(done)
    })

    it('should query or search - sort descending', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.sortDesc, { user })
        .then(docs => {
          console.log(docs)
          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()
          
          should.equal(docs.data[0].name, _data.result.sort[1].name)
          should.equal(docs.data[1].name, _data.result.sort[0].name)

          done()
        }).catch(done)
    })

    it('should query or search - limit (page 1)', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.limit, { user })
        .then(docs => {
          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()
          
          should.equal(docs.data[0].name, _data.result.sort[0].name)

          done()
        }).catch(done)
    })

    it('should query or search - limit (page 2)', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.page, { user })
        .then(docs => {
          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()

          should.equal(docs.data[0].name, _data.result.page[0].name)

          done()
        }).catch(done)
    })
    
    it(`should list all without pagination`, function (done) {
      let options = { listOnly: true, user }

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, options)
        .then(docs => {
          console.log(docs)
          should(docs).be.an.Array()
          should(docs).not.be.empty()
          done()
        }).catch(done)
    })

    it(`should count`, function (done) {
      let options = {}

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:count`, options)
        .then(result => {
          should(result.count).be.a.Number()
          should(result.count).be.greaterThanOrEqual(1)
          done()
        }).catch(done)
    })
  }) */
})
