'use strict'

const RESOURCE = 'roles'
global.Promise = require('bluebird')

const _ = require('lodash')
const _data = require('./_data')
const should = require('should')
const Utils = require('../utils')

const supertest = require('supertest')
const request = supertest('http://localhost:8080')

describe(`${_.startCase(RESOURCE)} API`, function () {
  let user = {}
  let config = {}
  let utils = new Utils(RESOURCE, [
    `svc:${RESOURCE},cmd:getperm`
  ])

  let data1
  let data2

  before(function (done) {
    utils.init('admin').then(options => {
      config = options.config
      user = options.user
    }).then(() => {
      _data.post.name = 'Dummy - Edited 2'
      return request.post(`/`).set(config).send(_data.post).then(ret => ret.body)
    }).then(data => {
      data1 = data
      _data.get.readId = data._id
      _data.post.name = 'Dummy for search'
      return request.post(`/`).set(config).send(_data.post).then(ret => ret.body)
    }).then(data => {
      data2 = data
      done()
    }).catch(done)
  })

  after(function (done) {
    Promise.all([
      request.delete(`/${data1._id}`).set(config),
      request.delete(`/${data2._id}`).set(config),
    ]).then(() => {
      done()
    }).catch(done)
  })

  describe('# Services - read', function () {
    it('should retrieve record by id - plane', function (done) {
      let options = {
        user: user,
        filter: {
          _id: _data.get.readId
        }
      }

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:read`, options)
        .then(doc => {
          should.exist(doc._id)
          should.equal(_data.get.readId, doc._id)
          done()
        }).catch(done)
    })

    it('should retrieve record by id - select', function (done) {
      let options = {
        user: user,
        filter: {
          _id: _data.get.readId
        }
      }

      options = Object.assign(options, _data.query.select)

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:read`, options)
        .then(doc => {
          should.exist(doc._id)
          should.exist(doc.isStandard)
          should.equal(_data.get.readId, doc._id)
          done()
        }).catch(done)
    })

    it('should retrieve record by id - deselect', function (done) {
      let options = {
        user: user,
        filter: {
          _id: _data.get.readId
        }
      }

      options = Object.assign(options, _data.query.deselect)

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:read`, options)
        .then(doc => {
          should.exist(doc._id)
          should.not.exist(doc.isStandard)
          should.equal(_data.get.readId, doc._id)

          done()
        }).catch(done)
    })
  })

  describe('# Services - query', function () {
    it('should query or search - plane', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, { user })
        .then(docs => {
          should.exist(docs.data)
          should.exist(docs.totalPages)
          should.exist(docs.currentPage)
          should.exist(docs.docsPerPage)
          should.exist(docs.totalRecords)

          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()
          should(docs.data).not.be.empty()
          should(docs.totalPages).be.greaterThanOrEqual(1)
          should(docs.totalRecords).be.greaterThanOrEqual(1)

          done()
        }).catch(done)
    })

    it('should query or search - select', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.select, { user })
        .then(docs => {
          should.exist(docs.data)
          should.exist(docs.totalPages)
          should.exist(docs.currentPage)
          should.exist(docs.docsPerPage)
          should.exist(docs.totalRecords)
          should.exist(docs.data[0].isStandard)
          done()
        }).catch(done)
    })

    it('should query or search - deselect', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.deselect, { user })
        .then(docs => {
          should.exist(docs.data)
          should.exist(docs.totalPages)
          should.exist(docs.currentPage)
          should.exist(docs.docsPerPage)
          should.exist(docs.totalRecords)
          should.not.exist(docs.data[0].name)
          should.not.exist(docs.data[0].isStandard)
          done()
        }).catch(done)
    })

    it('should query or search - sort ascending', function (done) {
      let options = {
        q: 'Dummy',
        user
      }
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.sortAsc, options)
        .then(docs => {
          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()
          
          should.equal(docs.data[0].name, _data.result.sort[0].name)
          should.equal(docs.data[1].name, _data.result.sort[1].name)

          done()
        }).catch(done)
    })

    it('should query or search - sort descending', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.sortDesc, { user })
        .then(docs => {
          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()
          
          should.equal(docs.data[0].name, _data.result.sort[1].name)
          should.equal(docs.data[1].name, _data.result.sort[0].name)

          done()
        }).catch(done)
    })

    it('should query or search - limit (page 1)', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.limit, { user })
        .then(docs => {
          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()
          
          should.equal(docs.data[0].name, _data.result.sort[0].name)
          done()
        }).catch(done)
    })

    it('should query or search - limit (page 2)', function (done) {
      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, _data.query.page, { user })
        .then(docs => {
          should(docs.data).be.an.Array()
          should(docs.totalPages).be.a.Number()
          should(docs.docsPerPage).be.a.Number()
          should(docs.currentPage).be.a.Number()
          should(docs.totalRecords).be.a.Number()

          should.equal(docs.data[0].name, _data.result.sort[1].name)

          done()
        }).catch(done)
    })

    it(`should list all without pagination`, function (done) {
      let options = { listOnly: true, user }

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:query`, options)
        .then(docs => {
          should(docs).be.an.Array()
          should(docs).not.be.empty()
          done()
        }).catch(done)
    })
  })

  describe('# Services - count', function () {
    it(`should count - plane`, function (done) {
      let options = { user }

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:count`, options)
        .then(result => {
          should(result.count).be.a.Number()
          should(result.count).be.greaterThanOrEqual(1)
          done()
        }).catch(done)
    })

    it(`should count - with query`, function (done) {
      let options = { user, q: 'Dummy' }

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:count`, options)
        .then(result => {
          should(result.count).be.a.Number()
          should(result.count).be.greaterThanOrEqual(1)
          done()
        }).catch(done)
    })
  })

  describe('# Services - getperm', function () {
    it('should retrieve permissions by id', function (done) {
      let options = {
        user: user,
        filter: {
          _id: _data.get.readId
        }
      }

      utils.serviceBus.invoke(`svc:${RESOURCE},cmd:getperm`, options)
        .then(doc => {
          should.equal(doc.accounts, _data.post.permissions[0].accessLevel)
          done()
        }).catch(done)
    })
  })
})
