'use strict'

// const raven = require('raven')
const Redis = require('ioredis')

class Cache {
  constructor (logger) {
    this.logger = logger
    this.caches = []
  }

  bootstrapSingleNode (isManaged = false) {
    return new Promise((resolve, reject) => {
      // this.logger.info('Connecting to Cache Server...')

      let options = {
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
        password: process.env.REDIS_PASSWORD,
        keepAlive: 5000,
        retryStrategy: function () {
          return 1000
        },
        reconnectOnError: function () {
          return true
        }
      }

      if (process.env.REDIS_SECURE === 'true') {
        Object.assign(options, {
          tls: {
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT,
            servername: process.env.REDIS_HOST
          }
        })
      }

      let redis = new Redis(options)

      redis.on('error', (err) => {
        this.logger.error(err)

        // raven.captureException(err, {
        //   level: 'fatal'
        // })

        reject(err)
      })

      redis.on('close', () => {
        this.logger.info('Cache client has disconnected.')
      })

      redis.on('end', () => {
        this.logger.info('Cache connection has ended.')
      })

      redis.on('reconnecting', () => {
        this.logger.info('Reconnecting to Cache Server...')
      })

      // redis.on('connect', () => {
      //   this.logger.info('Connected to Cache Server.')
      // })

      redis.once('ready', () => {
        // this.logger.info('Cache Server ready.')

        resolve(redis)
      })

      if (isManaged) {
        this.caches.push(redis)
      }
    })
  }

  bootstrapCluster (isManaged = false) {
    return new Promise((resolve, reject) => {
      this.logger.info('Connecting to Cache Cluster...')

      let hosts = `${process.env.REDIS_HOST || ''}`.split(',').filter(Boolean)
      let ports = `${process.env.REDIS_PORT || ''}`.split(',').filter(Boolean)
      let clusterNodes = []

      for (let i = 0; i < hosts.length; i++) {
        if (process.env.REDIS_SECURE === 'true') {
          clusterNodes.push({
            host: hosts[i],
            port: ports[i],
            password: process.env.REDIS_PASSWORD,
            tls: {
              host: hosts[i],
              port: ports[i]
            }
          })
        } else {
          clusterNodes.push({
            host: hosts[i],
            port: ports[i],
            password: process.env.REDIS_PASSWORD
          })
        }
      }

      let options = {
        password: process.env.REDIS_PASSWORD,
        keepAlive: 5000,
        retryStrategy: function () {
          return 1000
        },
        reconnectOnError: function () {
          return true
        }
      }

      let cluster = new Redis.Cluster(clusterNodes, {
        redisOptions: options
      })

      cluster.on('error', (err) => {
        this.logger.error(err)

        // raven.captureException(err, {
        //   level: 'fatal'
        // })

        reject(err)
      })

      cluster.on('close', () => {
        this.logger.info('Cache client has disconnected.')
      })

      cluster.on('end', () => {
        this.logger.info('Cache connection has ended.')
      })

      cluster.on('reconnecting', () => {
        this.logger.info('Reconnecting to Cache Cluster...')
      })

      cluster.on('connect', () => {
        this.logger.info('Connected to Cache Cluster.')
      })

      cluster.once('ready', () => {
        this.logger.info('Cache Cluster ready.')

        resolve(cluster)
      })

      if (isManaged) {
        this.caches.push(cluster)
      }
    })
  }

  bootstrap (isManaged = false) {
    let hosts = `${process.env.REDIS_HOST || ''}`.split(',').filter(Boolean)

    if (Array.isArray(hosts) && hosts.length > 1) {
      return this.bootstrapCluster(isManaged)
    } else {
      return this.bootstrapSingleNode(isManaged)
    }
  }

  close () {
    // this.logger.info('Closing Cache Server connection.')

    return Promise.each(this.caches, cache => {
      return new Promise(resolve => {
        cache.once('end', () => {
          resolve()
        })

        cache.quit()
      })
    })
  }
}

module.exports = Cache
