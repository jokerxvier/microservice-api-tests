'use strict'

process.env.RABBITMQ_URL = 'amqp://guest:guest@rabbitmq:5672'

const auth = require('./auth')
const seneca = require('seneca')

class Utils {
  constructor (resource, extraPins = []) {
    this.extraPins = extraPins
    this.resource = resource
  }

  toQueryStr (obj) {
    let str = '?'

    Object.keys(obj).forEach(key => {
      str += `${key}=${obj[key]}&`
    })

    return str.replace(/&$/, '')
  }

  init (permission) {
    return Promise.props({
      user: auth.getUser(this.resource, permission),
      token: auth.getToken(this.resource, permission)
    }).then(config => {
      let serviceBus = seneca()
      .use('seneca-amqp-transport')
      .client({
        type: 'amqp',
        url: process.env.RABBITMQ_URL,
        pin: [
          `svc:${this.resource},cmd:query`,
          `svc:${this.resource},cmd:count`,
          `svc:${this.resource},cmd:read`,
          `svc:${this.resource},cmd:create`,
          `svc:${this.resource},cmd:update`,
          `svc:${this.resource},cmd:patch`,
          `svc:${this.resource},cmd:delete`
        ].concat(this.extraPins).filter(Boolean).sort()
      })

      serviceBus.invoke = Promise.promisify(serviceBus.act)
      this.serviceBus = serviceBus
      
      return Promise.resolve({
        user: config.user,
        config: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${config.token}`
        }
      })
    })
  }
}

module.exports = Utils